require('dotenv').config()
const express = require('express')
const sequlize = require('./db')
const path = require('path')

const models = require('./models/models') // это только для того, чтобы создались автоматически таблицы и связи
const routes = require('./routes/index') // центральный роут
const errorHandler = require('./middleware/ErrorHandlingMiddleware') // это мидл для обработки ошибок

const cors = require('cors')

const fileUpload = require('express-fileupload')

const PORT = process.env.PORT || 5000

const app = express()

app.use(cors())
app.use(express.json())
app.use(express.static(path.resolve(__dirname, 'static'))) // to open images by http://localhost:5000/10a5a576-7741-4833-bd59-639924b153f7.jpg - example

app.use(fileUpload({})) // to upload files on server
app.use('/api', routes)

app.use(errorHandler) // мидл обработки ощибок идет последним, т.к. работа прекращается

const start = async () => {
  try {
    await sequlize.authenticate()
    await sequlize.sync()
    app.listen(PORT, () => {
      console.log(`Server started on port ${PORT}`)
    })
  } catch (er) {
    console.log(er)
  }
}

start()
