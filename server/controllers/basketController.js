const { User, Basket, BasketDevice, Device } = require('../models/models')
const ApiError = require('../error/ApiError')

class BasketController {
  //return basket id and product array to store mobx
  async getDevices(req, res, next) {
    const { userId } = req.params
    if (!userId) {
      return next(ApiError.badRequest('user id is not found'))
    }

    // const user = await User.findOne({ where: { id: userId }, include: {
    //   model: Basket,
    //   include: [BasketDevice]
    // } })
    let basket = await Basket.findOne({
      where: { userId },
      include: [
        {
          model: BasketDevice,
          as: 'devices',
          include: [{ model: Device, attributes: ['id', 'price'] }], //includes price 
          attributes: { exclude: ['createdAt', 'updatedAt'] },
        },
      ],
      attributes: { exclude: ['createdAt', 'updatedAt'] },
    })

    res.json(basket)
  }

  // create/update/delete device in basket
  async handleBasketDevice(req, res, next) {
    let { deviceId, basketId, quantity } = req.body
    if (!deviceId || !basketId) {
      return next(ApiError.badRequest('deviceId, basketId are required'))
    }

    const exists = await BasketDevice.findOne({
      where: { deviceId, basketId },
    })

    let result = null

    if (exists) {
      if (quantity < 1) {
        //or del if quantity === 0
        result = await BasketDevice.destroy({
          where: { deviceId, basketId },
        })
      } else {
        //update
        result = await BasketDevice.update(
          { quantity },
          { where: { deviceId, basketId } }
        )
        result = result[0]
      }
    } else {
      //create
      quantity = quantity || 1
      result = await BasketDevice.create({ deviceId, basketId, quantity })
      if (result) {
        result = 1
      }
    }

    res.json({ result })
  }

  async getBasketById(req, res, next) {
    let { basketId } = req.params
    if (!basketId) {
      return next(ApiError.badRequest('basketId is required'))
    }

    const result = await BasketDevice.findAll({ where: { basketId } })

    res.json(result)
  }

  async getDeviceBasketQuantity(req, res, next) {
    let { basketId, deviceId } = req.body
    if (!basketId || !deviceId) {
      return next(ApiError.badRequest('basketId, deviceId are required'))
    }

    const result = await BasketDevice.findOne({ where: { basketId, deviceId } })

    res.json(result)
  }
}

module.exports = new BasketController()
