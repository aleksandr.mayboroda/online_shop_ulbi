const { Brand } = require('../models/models')
const ApiError = require('../error/ApiError')

class BrandController {
  async create(req, res, next) {
    try {
      const { name } = req.body
      if (!name) {
        return next(ApiError.badRequest('name is required'))
      }

      const brand = await Brand.create({ name })
      return res.json(brand)
    } catch (error) {
      console.dir(error)
      next(new ApiError(error.message))
    }
  }

  async getAll(req, res, next) {
    const brands = await Brand.findAll()
    return res.json(brands)
  }
}

module.exports = new BrandController()
