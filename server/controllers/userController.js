const ApiError = require('../error/ApiError')
const bcrypt = require('bcrypt')
const { User, Basket } = require('../models/models')
const { generateJwt, comparePasswords } = require('../helpers/auth')

class UserController {
  async registration(req, res, next) {
    const { email, password, role } = req.body
    if (!email || !password) {
      return next(ApiError.badRequest('wrong email or password'))
    }

    const candidate = await User.findOne({ where: { email } })
    if (candidate) {
      return next(ApiError.badRequest(`user with email ${email} exists`))
    }
    const hashPass = await bcrypt.hash(password, 5) //5 - количество раз, которое он будет перехешировать пароль
    const user = await User.create({ email, password: hashPass, role })
    const basket = await Basket.create({ userId: user.id }) //create basket for user immidiately

    const token = generateJwt(user.id, user.email, user.role) //generate token
    res.json({ token })
  }

  async login(req, res, next) {
    const { email, password } = req.body
    if (!email || !password) {
      return next(ApiError.badRequest('wrong email or password'))
    }

    const user = await User.findOne({ where: { email } })
    if (!user) {
      return next(ApiError.badRequest('user is not found'))
    }

    const checkedPassword = comparePasswords(password, user.password)
    if (!checkedPassword) {
      return next(ApiError.badRequest('wrong email or password'))
    }

    const token = generateJwt(user.id, user.email, user.role) //generate token
    res.json({ token })
  }

  async check(req, res, next) {
    // const { id } = req.query
    // if (!id) {
    //   return next(ApiError.badRequest('id is not given'))
    // }
    const token = generateJwt(req.user.id, req.user.email, req.user.role)
    res.json({ token })
  }
}

module.exports = new UserController()
