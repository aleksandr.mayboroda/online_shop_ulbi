const { Type } = require('../models/models')
const ApiError = require('../error/ApiError')

class TypeController {
  async create(req, res, next) {
    const { name } = req.body

    if (!name) {
      return next(ApiError.badRequest('name is required'))
    }

    const type = await Type.create({ name })
    return res.json(type)
  }

  async getAll(req, res, next) {
    const types = await Type.findAll()
    return res.json(types)
  }

  async delete(req, res, next) {
    const {id} = req.params
    if (!id) {
      return next(ApiError.badRequest('id param is required'))
    }
    const isDeleted = await Type.destroy({where: {id}}) 
    res.json(isDeleted)
  }
}

module.exports = new TypeController()
