const { Rating } = require('../models/models')
const ApiError = require('../error/ApiError')

class RatingController {
  async create(req, res, next) {
    try {
      const { rate, userId, deviceId } = req.body
      if (!rate || !userId || !deviceId) {
        return next(ApiError.badRequest('rate, userId, deviceId are required'))
      }
      const rating = await Rating.create({ rate, userId, deviceId })
      return res.json(rating)
    } catch (error) {
      return next(ApiError.badRequest('my server error'))
    }
  }
}

module.exports = new RatingController()
