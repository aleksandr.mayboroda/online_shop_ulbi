const { Device, DeviceInfo, Type, Brand, Rating } = require('../models/models')
const ApiError = require('../error/ApiError')
const uuid = require('uuid')
const path = require('path')

class DeviceController {
  async create(req, res, next) {
    try {
      let { name, price, brandId, typeId, info } = req.body
      // const { img } = req.files

      if (!name || !price || !brandId || !typeId) {
        return next(
          ApiError.badRequest(
            'fields: name, price, brandId, typeId is required'
          )
        )
      }

      //file is required
      let fileName = ''
      if (!req.files || !'img' in req.files) {
        return next(ApiError.badRequest('image is required'))
      } else {
        const { img } = req.files
        fileName = uuid.v4() + '.jpg'
        img.mv(path.resolve(__dirname, '..', 'static', fileName))
      }

      const device = await Device.create({
        name,
        price,
        brandId,
        typeId,
        img: fileName,
      })

      if (info) {
        info = JSON.parse(info)
        info.forEach((element) => {
          //not async
          DeviceInfo.create({
            title: element.title,
            description: element.description,
            deviceId: device.id, // returns after creating device
          })
        })
      }

      return res.json(device)
    } catch (error) {
      // console.log(error)
      next(new ApiError(error.message))
    }
  }

  async getAll(req, res, next) {
    let { brandId, typeId, limit = 9, page = 1 } = req.query
    let offset = limit * page - limit
    let devices
    let options = {
      limit,
      offset,
      include: [
        { model: Type, attributes: ['name', 'id'] },
        { model: Brand, attributes: ['name', 'id'] },
        { model: Rating},
      ],
    }
    
    if (brandId && !typeId) {
      options.where = { brandId }
    }
    if (!brandId && typeId) {
      options.where = { typeId }
    }
    if (brandId && typeId) {
      options.where = { brandId, typeId }
    }

    devices = await Device.findAndCountAll(options)
    return res.json(devices)
  }

  async getOne(req, res, next) {
    const { id } = req.params
    if (!id) {
      return next(ApiError.badRequest('id is required'))
    }

    const device = await Device.findOne({
      where: { id },
      include: [{ model: DeviceInfo, as: 'info' }, { model: Rating}],
    })
    

    // const rating = await device.getRating()
    // console.log('aaa',rating)

    return res.json(device)
  }

  async getDevicesByIds(req, res, next) {
    let { devicesIds } = req.params
    if (!devicesIds) {
      return next(ApiError.badRequest('array devicesIds is required'))
    }

    devicesIds = devicesIds.split(',')

    const result = await Device.findAll({
      where: { id: devicesIds },
      include: [
        { model: Type, attributes: ['name', 'id'] },
        { model: Brand, attributes: ['name', 'id'] },
      ],
    })

    res.json(result)
  }
}

module.exports = new DeviceController()
