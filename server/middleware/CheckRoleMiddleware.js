const { tokenVerify } = require('../helpers/auth')

module.exports = function (role) {
  return function (req, res, next) {
    if (req.method === 'OPTIONS') {
      next()
    }
    try {
      const token = req.headers.authorization.split(' ')[1]
      if (!token) {
        return res.status(401).json({ message: 'user is not autorized' })
      }
      const decoded = tokenVerify(token)
      if(decoded.role !== role)
      {
        return res.status(403).json({ message: 'access denied' })
      }

      req.user = decoded //write user data for auto update token

      next()
    } catch (error) {
      res.status(401).json({ message: 'user is not autorized' })
    }
  }
}
