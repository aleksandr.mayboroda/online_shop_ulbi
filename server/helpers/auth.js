const jwt = require('jsonwebtoken')
const bcrypt = require('bcrypt')

// генерация токена, зашиваем параметры и ставим срок годности 24 часа
const generateJwt = (id, email, role) => {
  return jwt.sign({ id, email, role }, process.env.SECRET_KEY, {
    expiresIn: '24h',
  })
}

//сверка паролей
const comparePasswords = (userPass, useDbPass) =>
  bcrypt.compareSync(userPass, useDbPass)

//check token
const tokenVerify = token => {
  return jwt.verify(token, process.env.SECRET_KEY)
}

module.exports = {
  generateJwt,
  comparePasswords,
  tokenVerify,
}
