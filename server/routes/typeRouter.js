const Router = require('express')
const typeController = require('../controllers/typeController')
const checkRoleMiddleware = require('../middleware/CheckRoleMiddleware')

const router = new Router()

router.post('/', checkRoleMiddleware('ADMIN'), typeController.create) //check role here
router.get('/', typeController.getAll)
router.delete('/:id', checkRoleMiddleware('ADMIN'), typeController.delete)

module.exports = router
