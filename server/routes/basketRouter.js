const Router = require('express')
const basketController = require('../controllers/basketController')
const authMiddleware = require('../middleware/AuthMiddleware')

const router = new Router()

router.get('/:userId', authMiddleware, basketController.getDevices) //by user id
router.post('/handle', authMiddleware, basketController.handleBasketDevice) //handle basket devices

router.get('/basket/:basketId', authMiddleware, basketController.getBasketById) //get all basket devices by basket id

router.post('/device/quantity', authMiddleware, basketController.getDeviceBasketQuantity) //get current product quantity



module.exports = router
