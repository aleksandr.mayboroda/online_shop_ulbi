const Router = require('express')
const ratingController = require('../controllers/ratingController')

const router = new Router()

router.post('/', ratingController.create)
// router.get('/',deviceRouter.getAll)
// router.get('/:id',deviceRouter.getOne)
// router.get('/multiple/:devicesIds', deviceRouter.getDevicesByIds) //get multiple devices with comma (,)

module.exports = router
