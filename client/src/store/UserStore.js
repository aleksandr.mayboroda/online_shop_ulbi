import { makeAutoObservable } from 'mobx'

export default class UserStore {
  constructor() {
    this._isAuth = false //предполагаем, что эта переменная не будеть изменяться
    this._user = {}
    makeAutoObservable(this)
  }

  //actions
  setIsAuth(bool) {
    this._isAuth = bool
  }

  setUser(user) {
    this._user = user
  }

  logOut()
  {
    this._isAuth = false
    this._user = {}
  }

  //selectors
  get isAuth() {
    return this._isAuth
  }

  get user() {
    return this._user
  }

}
