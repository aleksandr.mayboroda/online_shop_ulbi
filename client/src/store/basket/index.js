import { makeAutoObservable } from 'mobx'
import {
  // getBasketById,
  getBasketByUserId,
  handleBasketDevice,
} from '../../http/api/basket'

export default class BasketStore {
  constructor() {
    this._data = []
    this._basketId = 0

    makeAutoObservable(this)
  }

  // actions
  setData(arr) {
    this._data = arr
  }

  setBasketId(id) {
    this._basketId = id
  }

  clear() {
    this.setData([])
    this.setBasketId(0)
  }

  // handle(id) {
  //   const index = this._data.findIndex((item) => item.id === id)

  //   if (index > -1) {
  //     if (this._data[index].quantity > 0) {
  //       this._data[index].quantity++
  //     }
  //   } else {
  //     this._data.push({ id, quantity: 1 })
  //   }
  // }

  // increment(id) {
  //   const index = this._data.findIndex((item) => item.id === id)
  //   if (index > -1) {
  //     this._data[index].quantity++
  //   } else {
  //     this._data.push({ id, quantity: 1 })
  //   }
  // }

  // decrement(id) {
  //   const index = this._data.findIndex((item) => item.id === id)
  //   console.log(id, this._data[index])
  //   if (index > -1) {
  //     if (this._data[index].quantity > 1) {
  //       this._data[index].quantity--
  //     } else {
  //       this._data = this._data.filter((item) => item.id !== id)
  //     }
  //   }
  // }

  async increment(device, quantity) {
    const deviceId = +device.id
    const { result } = await handleBasketDevice(
      this.basketId,
      deviceId,
      quantity
    )

    if (result) {
      let newData = []
      const index = this.data.findIndex((item) => item.id === deviceId)
      if (index > -1) {
        newData = [...this.data]
        newData[index].quantity = quantity
      } else {
        newData = [
          ...this.data,
          { id: deviceId, quantity: 1, price: device.price },
        ]
      }
      this.setData(newData)
    }
    return result
  }

  async decrement(device, quantity) {
    const deviceId = +device.id
    const { result } = await handleBasketDevice(
      this.basketId,
      deviceId,
      quantity
    )
    if (result) {
      let newData = []
      const index = this.data.findIndex((item) => item.id === deviceId)
      if (index > -1) {
        if (this.data[index].quantity > 0) {
          newData = [...this.data]
          newData[index].quantity = quantity
        } else {
          newData = this.data.filter((item) => item.id !== deviceId)
        }
      }
      if (!quantity) {
        newData = this.data.filter((item) => item.id !== deviceId)
      }
      this.setData(newData)
    }
    return result
  }

  checkInBasket(id) {
    const index = this.data.findIndex((item) => item.id === id)
    return index > -1 ? true : false
  }

  async getQuantity(id) {
    const elem = this.data.find((item) => item.id === id)
    return elem ? elem.quantity : 0
  }

  async loadBasketByUserId(userId) {
    if (userId) {
      const response = await getBasketByUserId(userId)
      this.setData(
        response.devices.map(({ quantity, device }) => ({
          id: device.id,
          quantity,
          price: device.price,
        }))
      )
      this.setBasketId(response.id)
    }
  }

  // async loadBasket() {
  //   if (this._basketId) {
  //     const { data } = await getBasketById(this._basketId)
  //     console.log(123,data)
  //     this._data = data.map(({ deviceId, quantity, price }) => ({
  //       id: deviceId,
  //       quantity,
  //       price,
  //     }))
  //   }
  // }

  // selectors
  get data() {
    return this._data
  }
  get basketId() {
    return this._basketId
  }

  get totalCount() {
    return this.data.reduce((acc, next) => {
      acc += next.quantity
      return acc
    }, 0)
  }

  get totalSum() {
    return this.data.reduce((acc, next) => {
      acc += next.quantity * next.price
      return acc
    }, 0)
  }
}
