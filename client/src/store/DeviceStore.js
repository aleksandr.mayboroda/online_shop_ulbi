import { makeAutoObservable } from 'mobx'
import { getTypes, getBrands, getDevices } from '../http/api/device'
import { createRating } from '../http/api/rating'

export default class DeviceStore {
  constructor() {
    this._types = []

    this._brands = []

    this._devices = []

    this._selectedType = {}

    this._selectedBrand = {}

    this._page = 1

    this._totalCount = 0

    this._limit = 6

    makeAutoObservable(this)
  }

  //actions
  setTypes(types) {
    this._types = types
  }

  setBrands(brands) {
    this._brands = brands
  }

  setDevices(devices) {
    this._devices = devices
  }

  setSelectedType(type) {
    this.setPage(1)
    this._selectedType = type
  }

  setSelectedBrand(brand) {
    this.setPage(1)
    this._selectedBrand = brand
  }

  setPage(value) {
    this._page = value
  }

  setTotalCount(value) {
    this._totalCount = value
  }

  setLimit(value) {
    this._limit = value
  }

  //async
  async fetchTypes() {
    try {
      const data = await getTypes()
      this.setTypes(data)
    } catch (error) {
      console.log('server error')
    }
  }

  async fetchBrands() {
    try {
      const data = await getBrands()
      this.setBrands(data)
    } catch (error) {
      console.log('server error')
    }
  }

  async fetchDevices() {
    try {
      const data = await getDevices(
        this.selectedType.id,
        this.selectedBrand.id,
        this.page,
        this.limit
      )
      this.setDevices(data.rows)
      this.setTotalCount(data.count)
    } catch (error) {
      console.log('server error')
    }
  }

  //create rating and update it in store for current device
  async addRating(newVal, userId, id) {
    try {
      const data = await createRating(newVal, userId, id)
      const newDevices = this.devices.map((device) =>
        device.id === id
          ? { ...device, ratings: [...device.ratings, data] }
          : device
      )
      this.setDevices(newDevices)
    } catch (error) {
      console.log('server error')
    }
  }

  //selectors
  get types() {
    return this._types
  }

  get brands() {
    return this._brands
  }

  get devices() {
    return this._devices
  }

  get selectedType() {
    return this._selectedType
  }

  get selectedBrand() {
    return this._selectedBrand
  }

  get page() {
    return this._page
  }

  get totalCount() {
    return this._totalCount
  }

  get limit() {
    return this._limit
  }
}
