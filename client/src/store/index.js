import UserStore from './UserStore'
import DeviceStore from './DeviceStore'
import BasketStore from './basket'

const store = {
  user: new UserStore(),
  device: new DeviceStore(),
  basket: new BasketStore(),
}

export default store
