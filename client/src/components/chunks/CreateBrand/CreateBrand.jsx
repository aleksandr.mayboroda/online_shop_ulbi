import { useState, useCallback } from 'react'
import { Modal, Form, Button, Alert } from 'react-bootstrap'
import { createBrand } from '../../../http/api/device'

const CreateBrand = ({ show, onHide }) => {
  const [value, setValue] = useState('')
  const [serverError, setServerError] = useState('')
  const onChange = (ev) => setValue(ev.target.value)

  const addBrand = useCallback(async () => {
    setServerError('')
    try {
      await createBrand({ name: value })
      setValue('')
      onHide()
    } catch (error) {
      if (error.response.data.message) {
        setServerError(error.response.data.message)
      }
    }
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [value])

  return (
    <>
      <Modal.Header closeButton>
        <Modal.Title>Add new brand</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <Form>
          <Form.Control
            placeholder="type name of brand"
            value={value}
            onChange={onChange}
          />
          {serverError && <Alert variant="danger">{serverError}</Alert>}
        </Form>
      </Modal.Body>
      <Modal.Footer>
        <Button variant="outline-danger" onClick={onHide}>
          Close
        </Button>
        <Button variant="outline-success" onClick={addBrand}>
          Create
        </Button>
      </Modal.Footer>
    </>
  )
}

export default CreateBrand
