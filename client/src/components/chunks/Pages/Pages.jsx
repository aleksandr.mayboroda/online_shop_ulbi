import React, { useContext, useCallback } from 'react'
import StoreContext from '../../../context/storeContext'
import { observer } from 'mobx-react-lite'

import { Pagination } from 'react-bootstrap'

const Pages = observer(() => {
  const { device } = useContext(StoreContext)

  const pages = []
  const pageCount = Math.ceil(device.totalCount / device.limit)

  // eslint-disable-next-line react-hooks/exhaustive-deps
  const changePage = useCallback((page) => device.setPage(page), [])

  for (let i = 0; i < pageCount; i++) {
    pages.push(i + 1)
  }

  const pagesList =
    !pages.length > 0
      ? null
      : pages.map((page) => (
          <Pagination.Item key={page} active={device.page === page} onClick={() => changePage(page)}>
            {page}
          </Pagination.Item>
        ))

  return <Pagination className="mt-5">{pagesList}</Pagination>
})

export default Pages
