import { useCallback } from 'react'
import { Modal, Button, Alert } from 'react-bootstrap'

import ReactStars from 'react-rating-stars-component'

const RatingModal = ({ onHide, item, rating, ratingChange }) => {
  const onChange = useCallback(
    (newVal) => {
      ratingChange(newVal)
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [item]
  )

  return (
    <>
      <Modal.Header closeButton>
        <Modal.Title>
          Set rating for{' '}
          <strong className="text-capitalize">{item.name}</strong>{' '}
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <Alert variant="warning">
          <Alert.Heading>Warning!</Alert.Heading>
          <p>You can set rating single time! Try to rhink about it!</p>
        </Alert>
        <div className="d-flex justify-content-center">
          <ReactStars
            count={5}
            size={40}
            value={rating}
            onChange={onChange}
            // edit={canChangeRating}
          />
        </div>
      </Modal.Body>
      <Modal.Footer>
        <Button variant="outline-danger" onClick={onHide}>
          Close
        </Button>
      </Modal.Footer>
    </>
  )
}

export default RatingModal
