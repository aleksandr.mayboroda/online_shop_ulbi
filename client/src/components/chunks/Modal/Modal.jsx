import React from 'react'
import { Modal } from 'react-bootstrap'

const BSModal = ({ onHide, data }) => {
  return (
    <Modal show={!!data} size="lg" centered onHide={onHide}>
      {data}
    </Modal>
  )
}

export default BSModal
