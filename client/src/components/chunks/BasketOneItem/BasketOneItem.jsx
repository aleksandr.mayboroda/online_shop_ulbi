import { useState } from 'react'
import { Col, Row, Card, Image, Button } from 'react-bootstrap'

import OneDeviceCounter from '../OneDeviceCounter/OneDeviceCounter'

const DOMAIN = process.env.REACT_APP_API_URI //for images path


/* Оптимизацию не делал, т.к. мобх с обсервером сам все оптимизирует */

const BasketOneItem = ({ item, basket }) => {
  const [num, setNum] = useState(item.quantity)

  const removeDevice = async () => {
    try {
      let newNum = 0
      const result = await basket.decrement(item, newNum)
      if (result) {
        setNum(newNum)
      }
    } catch (error) {
      if (error.response.data.message) {
        console.log(error.response.data.message)
      }
    }
  }

  const plusDevice = async () => {
    try {
      const newNum = num + 1
      const result = await basket.increment(item, newNum)
      if (result) {
        setNum(newNum)
      }
    } catch (error) {
      if (error.response.data.message) {
        console.log(error.response.data.message)
      }
    }
  }

  const minusDevice = async () => {
    try {
      let newNum = 0
      if (num > 0) {
        newNum = num - 1
      }
      const result = await basket.decrement(item, newNum)
      if (result) {
        setNum(newNum)
      }
    } catch (error) {
      if (error.response.data.message) {
        console.log(error.response.data.message)
      }
    }
  }

  return (
    <div key={item.id} className="py-1">
      <Card className="p-2">
        <Row className="align-items-center justify-content-between">
          <Col>
            <h6 className="text-capitalize">{item.brand.name}</h6>
          </Col>
          <Col>
            <h6 className="text-capitalize">{item.name}</h6>
          </Col>
          <Col>
            <h6 className="text-capitalize">{item.type.name}</h6>
          </Col>
          <Col>
            <h6 className="text-capitalize">price: {item.price}$</h6>
          </Col>
          <Col>
            <Image
              src={DOMAIN + item.img}
              alt={item.name}
              width={150}
              height={150}
            />
          </Col>
          <Col>
            <OneDeviceCounter
              counter={num}
              increment={plusDevice}
              decrement={minusDevice}
            />
          </Col>
          <Col>
            <Button
              variant="outline-danger"
              className="text-capitalize"
              onClick={removeDevice}
              disabled={!num > 0}
            >
              remove
            </Button>
          </Col>
        </Row>
        {/* {JSON.stringify(item)} */}
      </Card>
    </div>
  )
}

export default BasketOneItem
