import React from 'react'
import { Button } from 'react-bootstrap'

const OneDeviceCounter = ({ counter, increment, decrement }) => {
  return (
    <div className="d-flex justify-content-center align-items-center">
      <Button variant="danger" onClick={decrement} disabled={counter < 1}>
        -
      </Button>
      <span className="mx-2">{counter}</span>
      <Button variant="success" onClick={increment}>
        +
      </Button>
    </div>
  )
}

export default OneDeviceCounter
