import { useState, useCallback } from 'react'
import { Modal, Form, Button, Alert } from 'react-bootstrap'
import { createType } from '../../../http/api/device'

const CreateType = ({ show, onHide }) => {
  const [value, setValue] = useState('')
  const [serverError, setServerError] = useState('')
  const onChange = (ev) => setValue(ev.target.value)

  const addType = useCallback(async () => {
    setServerError('')
    try {
      await createType({ name: value })
      setValue('')
      onHide()
    } catch (error) {
      if (error.response.data.message) {
        setServerError(error.response.data.message)
      }
    }
  // eslint-disable-next-line react-hooks/exhaustive-deps
  },[value])

  return (
    <>
      <Modal.Header closeButton>
        <Modal.Title>Add new type</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <Form>
          <Form.Control
            placeholder="type name of type"
            value={value}
            onChange={onChange}
          />
        </Form>

        {serverError && <Alert variant="danger">{serverError}</Alert>}
      </Modal.Body>
      <Modal.Footer>
        <Button variant="outline-danger" onClick={onHide}>
          Close
        </Button>
        <Button variant="outline-success" onClick={addType}>
          Create
        </Button>
      </Modal.Footer>
    </>
  )
}

export default CreateType
