import React, { useState, useEffect, useContext, useCallback } from 'react'
import StoreContext from '../../../context/storeContext'
import { observer } from 'mobx-react-lite'
import { createDevice, getTypes, getBrands } from '../../../http/api/device'

import { Row, Col, Modal, Form, Button, Dropdown, Alert } from 'react-bootstrap'

const CreateDevice = observer(({ show, onHide }) => {
  const { device } = useContext(StoreContext)

  const [serverError, setServerError] = useState('')
  const [name, setName] = useState('')
  const [price, setPrice] = useState(0)
  const [file, setFile] = useState(null)
  const [info, setInfo] = useState([])

  useEffect(() => {
    getTypes().then((data) => device.setTypes(data))
    getBrands().then((data) => device.setBrands(data))
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  const clearFields = useCallback(() => {
    setServerError('')
    setName('')
    setPrice(0)
    setFile(null)
    setInfo([])
    device.setSelectedType({})
    device.setSelectedBrand({})
  },[])

  const addDevice = useCallback(async () => {
    setServerError('')
    try {
      let formData = new FormData()
      formData.append('name', name)
      formData.append('price', `${price}`)
      formData.append('typeId', device.selectedType.id)
      formData.append('brandId', device.selectedBrand.id)
      formData.append('img', file)
      formData.append('info', JSON.stringify(info))
      await createDevice(formData)
      clearFields()
      onHide()
    } catch (error) {
      if (error.response.data.message) {
        setServerError(error.response.data.message)
      }
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [name, price, device.selectedType.id, device.selectedBrand.id, file])

  const selectFile = useCallback((ev) => setFile(ev.target.files[0]),[])

  const addInfo = useCallback(() => {
    setInfo([...info, { title: '', description: '', number: Date.now() }])
  }, [info])

  const removeInfo = useCallback(
    (number) => {
      const filteredArr = info.filter((item) => item.number !== number)
      console.log('to del', number, filteredArr)
      setInfo(filteredArr)
    },
    [info]
  )

  const changeInfo = useCallback(
    (key, value, number) => {
      setInfo(
        info.map((item) =>
          item.number === number ? { ...item, [key]: value } : item
        )
      )
    },
    [info]
  )

  const typesList =
    !device.types.length > 0
      ? null
      : device.types.map((item) => (
          <Dropdown.Item
            key={item.id}
            onClick={() => device.setSelectedType(item)}
          >
            {item.name}
          </Dropdown.Item>
        ))

  const brandsList =
    !device.brands.length > 0
      ? null
      : device.brands.map((item) => (
          <Dropdown.Item
            key={item.id}
            onClick={() => device.setSelectedBrand(item)}
          >
            {item.name}
          </Dropdown.Item>
        ))

  const infoList =
    !info.length > 0
      ? null
      : info.map(({ number, title, description }) => (
          <Row key={number} className="my-2">
            <Col md={4}>
              <Form.Control
                placeholder="enter characteristics name"
                value={title}
                onChange={(ev) => changeInfo('title', ev.target.value, number)}
              />
            </Col>
            <Col md={4}>
              <Form.Control
                placeholder="enter characteristics description"
                value={description}
                onChange={(ev) =>
                  changeInfo('description', ev.target.value, number)
                }
              />
            </Col>
            <Col md={4}>
              <Button
                variant="outline-danger"
                onClick={() => removeInfo(number)}
              >
                Delete
              </Button>
            </Col>
          </Row>
        ))

  return (
    <>
      <Modal.Header closeButton>
        <Modal.Title>Add new device</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <Form>
          <Dropdown className="my-2">
            <Dropdown.Toggle>
              {device.selectedType.name || 'Select type'}
            </Dropdown.Toggle>
            <Dropdown.Menu>{typesList}</Dropdown.Menu>
          </Dropdown>
          <Dropdown className="my-2">
            <Dropdown.Toggle>
              {device.selectedBrand.name || 'Select brand'}{' '}
            </Dropdown.Toggle>
            <Dropdown.Menu>{brandsList}</Dropdown.Menu>
          </Dropdown>
          <Form.Control
            className="my-2"
            placeholder="enter device name"
            value={name}
            onChange={(ev) => setName(ev.target.value)}
          />
          <Form.Control
            className="my-2"
            placeholder="enter device price"
            type="number"
            value={price}
            onChange={(ev) => setPrice(Number(ev.target.value))}
          />
          <Form.Control
            className="my-2"
            placeholder="select device image"
            type="file"
            onChange={selectFile}
          />
          <Button variant="outline-dark" onClick={() => addInfo()}>
            Add more info
          </Button>
          {infoList}
        </Form>

        {serverError && (
          <Alert className="my-2" variant="danger">
            {serverError}
          </Alert>
        )}
      </Modal.Body>
      <Modal.Footer>
        <Button variant="outline-danger" onClick={onHide}>
          Close
        </Button>
        <Button variant="outline-success" onClick={addDevice}>
          Create
        </Button>
      </Modal.Footer>
    </>
  )
})

export default CreateDevice
