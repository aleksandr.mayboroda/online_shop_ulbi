import React, { useState, useCallback, useContext, useEffect } from 'react'
import { Col, Card, Image } from 'react-bootstrap'
import { useNavigate } from 'react-router-dom'
import { DEVICE_ROUTE } from '../../../routes/consts'
import { calculateRatingFromArr } from '../../../helpers/rating'
import StoreContext from '../../../context/storeContext'

import star from '../../../assets/images/star.png'
import ReactStars from 'react-rating-stars-component'

const DOMAIN = process.env.REACT_APP_API_URI //for images path

const DeviceItem = ({ item, userId }) => {
  const { id, name, img, brand, type, ratings } = item
  const navigate = useNavigate()
  const { device } = useContext(StoreContext)

  const [rating, setRating] = useState(() => calculateRatingFromArr(ratings))
  
  //if user can vote rating
  const canChangeRating = !!userId && !!!ratings.some((itm) => itm.userId === userId)

  const openCardLink = useCallback(
    () => navigate(DEVICE_ROUTE + '/' + id),
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [id]
  )

  const ratingChange = useCallback(async (newVal) => {
    if (userId) {
      //create new rating with userId
      //set current rating to state
      await device.addRating(newVal, userId, id)
    }
  // eslint-disable-next-line react-hooks/exhaustive-deps
  },[id, userId])

  useEffect(() => {
    setRating(calculateRatingFromArr(ratings))
  }, [ratings])

  return (
    <Col md={3} className="mt-3">
      <Card style={{ width: 150, cursor: 'pointer' }} border="light">
        <div onClick={openCardLink}>
          <Image src={DOMAIN + img} alt={name} width={150} height={150} />
          <div className="text-black-50 mt-1 d-flex justify-content-between align-items-center">
            <div className="text-capitalize">{brand.name}</div>
            <div className="d-flex align-items-center">
              <div className="me-1">{rating}</div>
              <Image src={star} alt="star" width={18} height={18} />
            </div>
          </div>
        </div>

        <div>
          <ReactStars
            count={5}
            size={25}
            value={rating}
            onChange={ratingChange}
            edit={canChangeRating}
          />
        </div>
        <div className="text-capitalize">{name}</div>
        <div className="text-capitalize text-black-50">{type.name}</div>
      </Card>
    </Col>
  )
}

export default DeviceItem
