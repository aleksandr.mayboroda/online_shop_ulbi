import React, { useContext, useCallback } from 'react'
import StoreContext from '../../../context/storeContext'
import { observer } from 'mobx-react-lite'
import { Card } from 'react-bootstrap'

const BrandBar = observer(() => {
  const { device } = useContext(StoreContext)

  const setSelected = useCallback(
    (brand) => device.setSelectedBrand(brand),
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [device.brands]
  )
  const isActive = useCallback(
    (id) => (id === device.selectedBrand.id ? 'danger' : 'light'),
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [device.brands]
  )

  const list =
    !device.brands.length > 0
      ? null
      : device.brands.map((brand) => (
          <Card
            key={brand.id}
            style={{ textTransform: 'capitalize', cursor: 'pointer' }}
            className="p-3"
            onClick={() => setSelected(brand)}
            border={isActive(brand.id)}
          >
            {brand.name}
          </Card>
        ))
        
  return <div className="d-flex flex-wrap">{list}</div>
})

export default BrandBar
