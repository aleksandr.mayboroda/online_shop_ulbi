import React, { useContext } from 'react'
import StoreContext from '../../../context/storeContext'
import { observer } from 'mobx-react-lite'
import { Row } from 'react-bootstrap'

import DeviceItem from '../../chunks/DeviceItem/DeviceItem'

const DeviceList = observer(() => {
  const { device, user } = useContext(StoreContext)

  const list =
    !device.devices.length > 0
      ? null
      : device.devices.map((item) => <DeviceItem key={item.id} item={item} userId={user.user.id || 0} />)

  return <Row className="d-flex flex-wrap">{list}</Row>
})

export default DeviceList
