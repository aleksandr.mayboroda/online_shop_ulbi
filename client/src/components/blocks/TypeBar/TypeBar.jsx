import React, { useContext, useCallback } from 'react'
import StoreContext from '../../../context/storeContext'
import { observer } from 'mobx-react-lite'
import { ListGroup } from 'react-bootstrap'

const TypeBar = observer(() => {
  const { device } = useContext(StoreContext)

  const setSelected = useCallback(
    (type) => device.setSelectedType(type),
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [device.types]
  )
  const isActive = useCallback(
    (id) => id === device.selectedType.id,
     // eslint-disable-next-line react-hooks/exhaustive-deps
    [device.types]
  )

  const list =
    !device.types.length > 0
      ? null
      : device.types.map((type) => (
          <ListGroup.Item
            key={type.id}
            style={{ textTransform: 'capitalize', cursor: 'pointer' }}
            onClick={() => setSelected(type)}
            active={isActive(type.id)}
          >
            {type.name}
          </ListGroup.Item>
        ))

  return <ListGroup>{list}</ListGroup>
})

export default TypeBar
