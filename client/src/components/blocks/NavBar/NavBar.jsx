import React, { useContext } from 'react'
import StoreContext from '../../../context/storeContext'
import { Container, Navbar, Nav, Button, Image, Badge } from 'react-bootstrap'
import { NavLink } from 'react-router-dom'
import {
  SHOP_ROUTE,
  ADMIN_ROUTE,
  LOGIN_ROUTE,
  BASKET_ROUTE,
} from '../../../routes/consts'
import { observer } from 'mobx-react-lite'
import { useNavigate } from 'react-router-dom'

import cartIcon from '../../../assets/svg/cartiIcon.svg'

const NavBar = observer(() => {
  const { user, basket } = useContext(StoreContext)
  const navigate = useNavigate()

  const logOut = () => {
    user.logOut()
    basket.clear()
    localStorage.removeItem('token')
    // navigate(LOGIN_ROUTE)
  }

  return (
    <Navbar bg="dark" variant="dark">
      <Container>
        <NavLink
          style={{ color: '#fff', textDecoration: 'none' }}
          to={SHOP_ROUTE}
        >
          ShopLogo
        </NavLink>
        {user.isAuth ? (
          <Nav className="ml-auto" style={{ color: '#fff' }}>
            {user.user.role === 'ADMIN' && (
              <Button
                variant="outline-light"
                onClick={() => navigate(ADMIN_ROUTE)}
              >
                Admin
              </Button>
            )}

            <Button
              variant="outline-light"
              className="ms-2 d-flex align-items-center"
              onClick={() => navigate(BASKET_ROUTE)}
            >
              <Image className='me-2' src={cartIcon} alt="cart icon" width={18} height={18} />
              <Badge bg="info" text="dark">{basket.totalCount}</Badge>
            </Button>

            <Button variant="outline-light" className="ms-2" onClick={logOut}>
              Exit
            </Button>
          </Nav>
        ) : (
          <Nav className="ml-auto" style={{ color: '#fff' }}>
            <Button
              variant="outline-light"
              onClick={() => navigate(LOGIN_ROUTE)}
            >
              Authorization
            </Button>
          </Nav>
        )}
      </Container>
    </Navbar>
  )
})

export default NavBar
