export const calculateRatingFromArr = (ratings) => {
  return !ratings.length > 0
    ? 0
    : ratings.reduce((acc, itm) => {
        acc += itm.rate
        return acc
      }, 0) / ratings.length
}
