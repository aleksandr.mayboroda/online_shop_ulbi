import React, { useContext } from 'react'
import { Routes, Route, Navigate } from 'react-router-dom'
import { authRoutes, pablicRoutes, adminRoutes } from './routes'
import { observer } from 'mobx-react-lite'
import StoreContext from '../context/storeContext'

const AppRoutes = observer(() => {
  const { user } = useContext(StoreContext)

  const publicRoutesList =
    !pablicRoutes.length > 0
      ? null
      : pablicRoutes.map(({ path, Component }) => (
          <Route key={path} path={path} element={<Component />} />
        ))

  const authRoutesList =
    !authRoutes.length > 0
      ? null
      : authRoutes.map(({ path, Component, redirect }) => (
          <Route
            key={path}
            path={path}
            element={
              user.isAuth ? (
                <Component />
              ) : (
                <Navigate to={redirect} state={{ redirectedFrom: path }} />
              )
            }
          />
        ))

  const adminRoutesList =
    !adminRoutes.length > 0
      ? null
      : adminRoutes.map(({ path, Component, redirect }) => (
          <Route
            key={path}
            path={path}
            element={
              user.isAuth && user.user.role === 'ADMIN' ? (
                <Component />
              ) : (
                <Navigate to={redirect} state={{ redirectedFrom: path }} />
              )
            }
          />
        ))

  return (
    <Routes>
      {authRoutesList}
      {publicRoutesList}
      {adminRoutesList}
    </Routes>
  )
})

export default AppRoutes
