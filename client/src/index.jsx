import React from 'react'
import ReactDOM from 'react-dom/client'
import App from './App'
import reportWebVitals from './reportWebVitals'
import { BrowserRouter } from 'react-router-dom'
import StoreContext from './context/storeContext'
import store from './store'
import ModalContextProvider from './context/modalContext'

const root = ReactDOM.createRoot(document.getElementById('root'))
root.render(
  <React.StrictMode>
    <StoreContext.Provider value={store}>
      <BrowserRouter basename={process.env.PUBLIC_URL}>
        <ModalContextProvider>
          <App />
        </ModalContextProvider>
      </BrowserRouter>
    </StoreContext.Provider>
  </React.StrictMode>
)

reportWebVitals()
