import { createContext, useState, useCallback } from 'react'
import BSModal from '../components/chunks/Modal/Modal'

const initialValue = {
  data: null,
}

export const ModalContext = createContext(initialValue)

const ModalContextProvider = ({ children }) => {
  const [content, setContent] = useState(initialValue.data)
  
  const modalToggle = useCallback((newValue) => setContent(newValue), [])
  const modalHide = useCallback(() => setContent(null), [])

  return (
    <ModalContext.Provider value={{ modalHide, modalToggle, setContent }}>
      {children}
      <BSModal onHide={modalHide} data={content} />
    </ModalContext.Provider>
  )
}

export default ModalContextProvider
