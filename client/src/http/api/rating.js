import { $authHost } from '../index'

export const createRating = async (rate, userId, deviceId) => {
  const { data } = await $authHost.post('api/rating', {rate, userId, deviceId})
  return data
}