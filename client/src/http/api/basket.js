import { $authHost } from '../index'

export const getBasketByUserId = async (userId) => {
  const { data } = await $authHost.get(`api/basket/${userId}`)
  return data
}

export const handleBasketDevice = async (basketId, deviceId, quantity) => {
  const { data } = await $authHost.post(`api/basket/handle`, {
    basketId,
    deviceId,
    quantity,
  })
  return data
}

export const getBasketById = async (basketId) => {
  const result = await $authHost.get(`api/basket/basket/${basketId}`)
  return result
}

export const getDeviceQuantity = async (basketId, deviceId) => {
  const {data} = await $authHost.post(`api/basket/device/quantity`, {
    basketId,
    deviceId,
  })
  return data
}