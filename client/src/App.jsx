import { useState, useEffect, useContext } from 'react'
import StoreContext from './context/storeContext'
import { observer } from 'mobx-react-lite'
import { check } from './http/api/user'
import { useLocation, useNavigate } from 'react-router-dom'

import { Spinner } from 'react-bootstrap'
import AppRoutes from './routes/AppRoutes'
import NavBar from './components/blocks/NavBar/NavBar'

const App = observer(() => {
  const location = useLocation()
  const navigate = useNavigate()

  const { user, basket } = useContext(StoreContext)
  const [loading, setLoading] = useState(false)

  //автоматическая проверка токена
  useEffect(() => {
    setLoading(true)
    check()
      .then((data) => {
        user.setUser(data)
        user.setIsAuth(true)

        //basket save
        basket.loadBasketByUserId(data.id)
      })
      .catch((err) => {
        return true
      })
      .finally(() => {
        setLoading(false)
      })
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  //проверка авторизации и перевод на закрытую страницу, на которой был до этого
  useEffect(() => {
    if (user.isAuth) {
      if (location.state?.redirectedFrom) {
        navigate(location.state?.redirectedFrom)
      }
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [user.isAuth])

  // console.log('user', basket)

  if (loading) {
    return <Spinner animation="grow" />
  }

  return (
    <div>
      <NavBar />
      <AppRoutes />
    </div>
  )
})

export default App
