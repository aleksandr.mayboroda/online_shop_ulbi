import React, { useContext, useEffect } from 'react'
import { Container, Row, Col } from 'react-bootstrap'
import StoreContext from '../context/storeContext'
import { observer } from 'mobx-react-lite'

import TypeBar from '../components/blocks/TypeBar/TypeBar'
import BrandBar from '../components/blocks/BrandBar/BrandBar'
import DeviceList from '../components/blocks/DeviceList/DeviceList'
import Pages from '../components/chunks/Pages/Pages'

const Shop = observer(() => {
  const { device } = useContext(StoreContext)
  const getTypes = async () => await device.fetchTypes()
  const getBrands = async () => await device.fetchBrands()
  const getDevices = async () => await device.fetchDevices()

  useEffect(() => {
    getTypes()
    getBrands()
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  useEffect(() => {
    getDevices()
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [device.page, device.selectedType, device.selectedBrand])

  return (
    <Container>
      <Row className="mt-2">
        <Col md="3">
          <TypeBar />
        </Col>
        <Col md="9">
          <BrandBar />
          <DeviceList />
          <Pages />
        </Col>
      </Row>
    </Container>
  )
})

export default Shop
