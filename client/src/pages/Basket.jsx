import { useState, useEffect, useContext } from 'react'
import { Container } from 'react-bootstrap'
import StoreContext from '../context/storeContext'
import { observer } from 'mobx-react-lite'
import { getDevicesByIds } from '../http/api/device'

import BasketOneItem from '../components/chunks/BasketOneItem/BasketOneItem'

const Basket = observer(() => {
  const { basket } = useContext(StoreContext)
  const [data, setData] = useState([])

  const loadData = async () => {
    const devicesIds = basket.data.map(({ id }) => id)
    if (devicesIds.length > 0) {
      const result = await getDevicesByIds(devicesIds)
      const newArray = result.map((item) => ({
        ...item,
        quantity:
          basket.data.find((bask) => item.id === bask.id)?.quantity || 0,
      }))
      setData(newArray)
    } else {
      setData([])
    }
  }

  useEffect(() => {
    loadData()
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [basket.data])

  const list =
    !data.length > 0 ? (
      <div className="d-flex justify-content-center h-100">
        <h4 className="text-capitalize mt-5">basket is empty</h4>
      </div>
    ) : (
      data.map((item) => (
        <BasketOneItem key={item.id} item={item} basket={basket} />
      ))
    )

  const totalCount =
    !data.length > 0 ? null : (
      <div className="text-end p-2">
        <p>
          Total quantity: <strong>{basket.totalCount}</strong> devices
        </p>
        <p>
          Total sum: <strong>{basket.totalSum}</strong>$
        </p>
      </div>
    )

  return (
    <Container className="mt-3">
      <h1 className="text-center">Basket page</h1>
      <div className="d-flex flex-column">{list}</div>
      {totalCount}
    </Container>
  )
})

export default Basket
