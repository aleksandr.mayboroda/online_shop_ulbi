import React, { useState, useContext } from 'react'
import { NavLink, useLocation, useNavigate } from 'react-router-dom'
import { Container, Card, Form, Button, Alert } from 'react-bootstrap'
import { REGISTRATION_ROUTE, LOGIN_ROUTE } from '../routes/consts'
import { registartion, login } from '../http/api/user'
import StoreContext from '../context/storeContext'
import { observer } from 'mobx-react-lite'
import { SHOP_ROUTE } from '../routes/consts'

const Auth = observer(() => {
  const location = useLocation()
  const navigate = useNavigate()
  const isLoginComponent = location.pathname === LOGIN_ROUTE

  const { user, basket } = useContext(StoreContext)

  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')

  const [serverError, setServerError] = useState('')

  const setEmailValue = (ev) => setEmail(ev.target.value)
  const setPasswordValue = (ev) => setPassword(ev.target.value)

  const clearFields = () => {
    setEmail('')
    setPassword('')
    setServerError('')
  }

  const auth = async () => {
    setServerError('')
    let data = {}
    try {
      if (isLoginComponent) {
        data = await login(email, password)
      } else {
        data = await registartion(email, password)
      }
      user.setUser(data)
      user.setIsAuth(true)
      navigate(
        location.state?.redirectedFrom
          ? location.state.redirectedFrom
          : SHOP_ROUTE
      )
      clearFields()
      basket.loadBasketByUserId(data.id)
    } catch (error) {
      console.log(error)
      if (error.response?.data.message) {
        setServerError(error.response.data.message)
      }
    }
  }

  return (
    <Container
      className="d-flex justify-content-center align-items-center"
      style={{ height: window.innerHeight - 54 }}
    >
      <Card style={{ width: 600 }} className="p-5">
        <h2 className="m-auto">
          {isLoginComponent ? 'Authorization' : 'Registration'}
        </h2>
        <Form className="d-flex flex-column">
          <Form.Control
            className="my-3"
            placeholder="enter your email"
            value={email}
            onChange={setEmailValue}
          />
          <Form.Control
            className="my-3"
            type="password"
            placeholder="enter your password"
            value={password}
            onChange={setPasswordValue}
          />

          {serverError && <Alert variant="danger">{serverError}</Alert>}

          <div className="d-flex justify-content-between flex-row align-items-center">
            <div>
              Don't have an account?{' '}
              {isLoginComponent ? (
                <NavLink
                  to={REGISTRATION_ROUTE}
                  style={{ textDecoration: 'none' }}
                >
                  Register
                </NavLink>
              ) : (
                <NavLink to={LOGIN_ROUTE} style={{ textDecoration: 'none' }}>
                  Authorize
                </NavLink>
              )}
            </div>

            <Button variant="outline-success" onClick={auth}>
              Enter
            </Button>
          </div>
        </Form>
      </Card>
    </Container>
  )
})

export default Auth
