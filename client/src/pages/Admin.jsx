import React, { useState, useCallback, useContext } from 'react'
import { Container, Button } from 'react-bootstrap'
import { ModalContext } from '../context/modalContext'

import CreateType from '../components/chunks/CreateType/CreateType'
import CreateBrand from '../components/chunks/CreateBrand/CreateBrand'
import CreateDevice from '../components/chunks/CreateDevice/CreateDevice'

const Admin = () => {
  const { modalHide, modalToggle } = useContext(ModalContext)

  const [showModal, setShowModal] = useState(false)
  // const handleModal = useCallback((id) => setShowModal(id), [])
  const handleModal = useCallback((id) => {
    setShowModal(id)
    let content = null
    if(id === 1)
    {
      content = <CreateType onHide={modalHide} />
    }
    if(id === 2)
    {
      content = <CreateBrand onHide={modalHide} />
    }
    if(id === 3)
    {
      content = <CreateDevice onHide={modalHide} />
    }
    modalToggle(content)

  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [showModal])

  return (
    <Container className="d-flex flex-column ">
      <Button
        variant="outline-dark"
        className="mt-4 p-2"
        onClick={() => handleModal(1)}
      >
        Add type
      </Button>
      <Button
        variant="outline-dark"
        className="mt-4 p-2"
        onClick={() => handleModal(2)}
      >
        Add brand
      </Button>
      <Button
        variant="outline-dark"
        className="mt-4 p-2"
        onClick={() => handleModal(3)}
      >
        Add device
      </Button>
    </Container>
  )
}

export default Admin
