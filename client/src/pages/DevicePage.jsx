import { useState, useEffect, useContext, useCallback } from 'react'
import { Container, Col, Row, Card, Image, Button } from 'react-bootstrap'
import { useParams, useLocation, useNavigate } from 'react-router-dom'
import { getOneDevice } from '../http/api/device'
import { getDeviceQuantity } from '../http/api/basket'
import { createRating } from '../http/api/rating'
import { calculateRatingFromArr } from '../helpers/rating'

import StoreContext from '../context/storeContext'
import { observer } from 'mobx-react-lite'

import { ModalContext } from '../context/modalContext'
import { LOGIN_ROUTE } from '../routes/consts'

import bigStar from '../assets/images/bigStar.png'

import OneDeviceCounter from '../components/chunks/OneDeviceCounter/OneDeviceCounter'
import RatingModal from '../components/modals/RatingModal/RatingModal'

const DevicePage = observer(() => {
  const location = useLocation()
  const navigate = useNavigate()
  const { id } = useParams()
  const [item, setItem] = useState({ info: [], ratings: [] })
  const [num, setNum] = useState(0)
  const [rating, setRating] = useState(() =>
    calculateRatingFromArr(item.ratings)
  )

  const { modalHide, modalToggle } = useContext(ModalContext)
  const { user, basket, device } = useContext(StoreContext)

  //при каждом изменении массива рейтинга проверяем, не оставлял ли пользователь рейтинг текущему товару
  const checkRights = useCallback(
    () =>
      !!user.user.id &&
      (!item.ratings.some((itm) => itm.userId === user.user.id) ||
        !item.ratings.length),
    [user.user.id, item.ratings]
  )

  const [canChangeRating, setCanChangeRating] = useState(() => checkRights)

  const openModal = () => {
    if (canChangeRating) {
      modalToggle(
        <RatingModal
          onHide={modalHide}
          item={item}
          device={device}
          user={user.user}
          rating={rating}
          ratingChange={ratingChange}
        />
      )
    } else {
      navigate(LOGIN_ROUTE, { state: { redirectedFrom: location.pathname } })
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }

  const ratingChange = useCallback(
    async (newVal) => {
      try {
        const userId = user.user.id
        if (userId) {
          //create new rating with userId
          //set current rating to state
          const data = await createRating(newVal, userId, item.id)
          setItem((prev) => ({ ...prev, ratings: [...prev.ratings, data] }))
          modalHide()
          setCanChangeRating(false)
        }
      } catch (error) {
        if (error.response.data.message) {
          console.log(error.response.data.message)
        }
      }
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [user.user.id, item]
  )

  const removeDevice = useCallback(async () => {
    try {
      let newNum = 0
      const result = await basket.decrement(item, newNum)
      if (result) {
        setNum(newNum)
      }
    } catch (error) {
      if (error.response.data.message) {
        console.log(error.response.data.message)
      }
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [item, num])

  const plusDevice = useCallback(async () => {
    try {
      const newNum = num + 1
      console.log(item, newNum)
      const result = await basket.increment(item, newNum)
      if (result) {
        setNum(newNum)
      }
    } catch (error) {
      if (error.response.data.message) {
        console.log(error.response.data.message)
      }
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [item, num, id])

  const minusDevice = useCallback(async () => {
    try {
      let newNum = 0
      if (num > 0) {
        newNum = num - 1
      }
      const result = await basket.decrement(item, newNum)
      if (result) {
        setNum(newNum)
      }
    } catch (error) {
      if (error.response.data.message) {
        console.log(error.response.data.message)
      }
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [item, num])

  const addButtonHandler = useCallback(() => {
    if (user.isAuth) {
      num > 0 ? removeDevice() : plusDevice()
    } else {
      navigate(LOGIN_ROUTE, { state: { redirectedFrom: location.pathname } })
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [num, user.isAuth, removeDevice, plusDevice])

  const loadDevice = useCallback(async () => {
    try {
      const data = await getOneDevice(id)
      const ratingNum =
        !data.ratings.length > 0
          ? 0
          : data.ratings.reduce((acc, itm) => {
              acc += itm.rate
              return acc
            }, 0) / data.ratings.length
      setRating(ratingNum)
      setItem(data)
    } catch (error) {
      if (error.response.data.message) {
        console.error(error.response.data.message)
      }
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  const loadQuantity = useCallback(async () => {
    if (basket.basketId > 0) {
      const result = await getDeviceQuantity(basket.basketId, id)
      if (result) {
        setNum(result.quantity)
      }
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  useEffect(() => {
    setRating(calculateRatingFromArr(item.ratings))
    setCanChangeRating(checkRights())
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [item.ratings])

  useEffect(() => {
    loadDevice()
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  useEffect(() => {
    loadQuantity()
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [basket.basketId])

  const descriptionList =
    !item.info.length > 0
      ? null
      : item.info.map(({ id, title, description }, index) => (
          <Row
            key={id}
            style={{
              backgroundColor: index % 2 === 0 ? 'lightgray' : 'transparent',
              padding: 10,
            }}
          >
            {title}: {description}
          </Row>
        ))

  return (
    <Container className="mt-3">
      <Row>
        <Col md={4}>
          <Image
            src={process.env.REACT_APP_API_URI + item.img}
            alt={item.name}
            width={300}
            height={300}
          />
        </Col>
        <Col md={4}>
          <div className="d-flex flex-column align-items-center">
            <h2>{item.name}</h2>
            <div
              className="d-flex align-items-center justify-content-center"
              style={{
                background: `url(${bigStar}) no-repeat center center`,
                width: 240,
                height: 240,
                backgroundSize: 'cover',
                fontSize: 64,
                cursor: 'pointer',
              }}
              onClick={openModal}
            >
              {rating}
            </div>
          </div>
        </Col>
        <Col md={4}>
          <Card
            className="d-flex flex-column justify-content-around align-items-center"
            style={{
              width: 300,
              height: 300,
              fontSize: 32,
              border: '5px solid lightgray',
            }}
          >
            <h3>From: {item.price} $</h3>
            <Button variant="outline-dark" onClick={addButtonHandler}>
              {user.isAuth && num > 0 ? 'Delete from cart' : 'Add to cart'}
            </Button>
            {user.isAuth && num > 0 && (
              <OneDeviceCounter
                counter={num}
                increment={plusDevice}
                decrement={minusDevice}
              />
            )}
          </Card>
        </Col>
      </Row>
      <Row className="d-flex flex-column m-3">
        <h1>Characteristics:</h1>
        {descriptionList}
      </Row>
    </Container>
  )
})

export default DevicePage
